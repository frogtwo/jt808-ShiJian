#coding:utf-8
import requests
import json


class APItest(object):
	pass

	def __init__(self, baseurl):
		self.baseurl = baseurl
		self.headers = {'content-type': 'application/json'}
		self.token = None


	def login(self, phone_number, password, path=r'/login'):
		'''
		@
		'''
		payload = {'phone_number':phone_number, 'password':password}
		response = requests.post(url=self.baseurl+path, headers=self.headers, data=json.dumps(payload))
		response_data = json.loads(response.content)
		self.token = response_data.get('token')

		return response_data


	def register(self, phone_number, password,path=r'/register'):
		'''
		@
		'''
		payload = {'phone_number':phone_number, 'password':password}
		response = requests.post(url=self.baseurl+path, headers=self.headers, data=json.dumps(payload))
		response_data = json.loads(response.content)

		return response_data

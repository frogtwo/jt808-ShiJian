import logging


log = logging.getLogger("JT808")
log.setLevel(logging.INFO)

fh = logging.FileHandler("./jt808Wasv.log")
fh.setLevel(logging.INFO)

ch =logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s---%(levelname)s---%(message)s')

fh.setFormatter(formatter)
ch.setFormatter(formatter)

log.addHandler(fh)
log.addHandler(ch)
import sys
sys.path.append('../')


class LocationEntity(object):

	'''
	业务实体类 位置《entity》- 对应JT808消息体相关
	'''

	terminalPhone='' #终端手机号
	alarm=0
	statusField=0
	latitude=0
	longitude=0
	elevation=0
	speed=0
	direction=0
	time=''


	def parseFromLocationMsg(self):
		''' 将接收的msg 转换成类实例属性值'''
		pass
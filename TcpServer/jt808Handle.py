import sys
sys.path.append('../')



class BaseHandle(object):

	'''
	@
	'''

	message_dict = {
		0x0100: RegisteHandle,		# 终端注册
		0x8100: RegisteRespHandle,		# 注册应答
		0x0102: AuthHandle,			# 终端鉴权
		0x8001: CommonRespHandle,		# 平台通用应答 
		0x0002: HeartBeatHandle,		# 终端心跳
		0x0003: LogoutHandle,		# 终端注销
		0x0001: TerminalRespHandle,		# 终端通用应答
		0x0200: LocationHandle,		# 终端位置
		}
	


class RegisteHandle(BaseHandle):

	'''
	@
	'''
	def __init__(self, content):
		self.content = content




class AuthHandle(BaseHandle):

	'''
	@
	'''

	def __init__(self, content):
		self.content = content



class RegisteRespHandle(BaseHandle):

	'''
	@
	'''
	
	def __init__(self, content):
		self.content = content



class TerminalRespHandle(BaseHandle):

	'''
	@
	'''
	
	def __init__(self, content):
		self.content = content



class LocationHandle(BaseHandle):

	'''
	@
	'''
	
	def __init__(self, content):
		self.content = content



class HeartBeatHandle(BaseHandle):

	'''
	@
	'''

	def __init__(self, content):
		self.content = content



class LogoutHandle(BaseHandle):

	'''
	@
	'''

	def __init__(self, content):
		self.content = content
	

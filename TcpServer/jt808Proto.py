
import sys
sys.path.append('../')


import struct, binascii

from bitstring import BitArray



class BaseProto(object):

	pass
	'''
	@协议基类
	'''



class Header(BaseProto):
	
	''' 
		消息头类
		定义报文 Header： msgId + msgBodyProps + termPhone + flowId
		808 消息头中，《消息体属性》 解析:包长度第 6-15bit, 加密类型第 5bit, 是否分包 第 2bit
	'''

	msgId = BitArray(length=8*2)	# 8bit*2 = 2byte
	msgBodyProps = BitArray(length=8*2)
	termPhone = BitArray(length=8*6)
	flowId = BitArray(length=8*6)


	def getMsgBodyLength(self):
		return self.msgBodyProps & 0x3ff # 11 11111111 (0-9bit)


	def getEncryption(self):
		return self.msgBodyProps & 0x1c00 >> 10 # 11100 00 00 00 00（10-12bit)


	def  hasSubPackage(self):
		return self.msgBodyProps & 0x2000 >> 13 # 10000 00 00 00 00 （13bit)



class RegisteResp(BaseProto):

	'''
		终端注册应答
	'''

	commandId = 0x8100
	structParams = [
		('Seq','>H',0),
		('Result','>B',0),
		('Auth','>8B',''),
	]

	def parseBody(self,):
		pass
		'''
		@
		'''



class CommonResp(BaseProto):
	
	'''
		平台通用应答
	'''

	commandId = 0x8001
	structParams = [
		('Seq','>H',0),
		('MsgID','>H',0),	#对应终端发上来的 commandId
		('Result','>B',0),
	]

	def parseBody(self,):
		pass
		'''
		@
		'''



class TerminalResp(BaseProto):
	
	'''
		终端通用应答
	'''

	commandId = 0x0001
	structParams = [
		('Seq','>H',0),
		('MsgID','>H',0),
		('Result','>B',0),
	]

	def parseBody(self,):
		pass
		'''
		@
		'''



class Registe(BaseProto):
	
	'''
		终端注册
	'''


	def __init__(self, byteBuf):

		'''
		@param: byteBuf是除去消息头的原始数据
		'''
		self.byteBuf = byteBuf

	# requireAck = TerminalResp
	# commandId = 0x0100
	provinceId = ('>H',0)
	cityId = ('>H',0)
	manufacturerId = ('>5B','')
	terminalType = ('>20B','')
	terminalId = ('>7B','')
	licensePlateColor = ('>B', 0)
	licensePlate = ('>8B','')

	def parseBody(self):

		'''
		@param： pos是byteBuf的索引位置
		'''
		pos = 0

		self.provinceId = struct.unpack( provinceId[0], self.byteBuf[pos, pos+struct.calcsize(provinceId[0])] )
		self.cityId = struct.unpack( cityId[0],  self.byteBuf[pos, pos+struct.calcsize(cityId[0])] )
		self.manufacturerId = struct.unpack( manufacturerId[0],  self.byteBuf[pos, pos+struct.calcsize(manufacturerId[0])] )
		self.terminalType = struct.unpack( terminalType[0],  self.byteBuf[pos, pos+struct.calcsize(terminalType[0])] )
		self.terminalId = struct.unpack( terminalId[0],  self.byteBuf[pos, pos+struct.calcsize(terminalId[0])] )
		self.licensePlateColor = struct.unpack( licensePlateColor[0],  self.byteBuf[pos, pos+struct.calcsize(licensePlateColor[0])] )
		self.licensePlate = struct.unpack( licensePlate[0],  self.byteBuf[pos, pos+struct.calcsize(licensePlate[0])] )



class Auth(BaseProto):
	
	'''
		终端鉴权
	'''
	requireAck = CommonResp
	commandId = 0x0102
	structParams = [
		('Auth','>8B',''),
	]

	def parseBody(self,):
		pass
		'''
		@
		'''



class HeartBeat(BaseProto):
	
	'''
		终端心跳
	'''

	requireAck = CommonResp
	commandId = 0x0003

	def parseBody(self,):
		pass
		'''
		@
		'''



class Location(BaseProto):
	
	'''
		实时位置
	'''

	requireAck = CommonResp
	commandId = 0x0200
	structParams = [
		('SOS','>L',0),
		('Status','>L',0),
		('Lat','>L',0),
		('Lng','>L',0),
		('Height','>H',0),
		('Speed','>H',0),
		('Direction','>H',0),
		('Time','>6B','BCD'),
		# 位置附加信息项，附加信息长度id、长度根据jt808表27定义
		('AddMsgId','>B',''),
		('AddMsgLen','>B',''),
		('AddMsgNote','>4B','')		# 默认解析里程
	]

	def parseBody(self,):
		pass
		'''
		@
		'''



class Logout(BaseProto):
	
	'''
		终端注销
	'''

	requireAck = CommonResp
	commandId = 0x0003

	def parseBody(self,):
		pass
		'''
		@
		'''



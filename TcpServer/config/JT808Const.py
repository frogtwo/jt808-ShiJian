from enum import Enum, unique


@unique
class JT808const(Enum):

	#消息分隔符
	PKG_DELIMITER = 0x7e;

	# 终端应答
	TERNIMAL_RESP_COMMON_ = 0x0001		#通用应答

	# 终端消息分类
	TERNIMAL_MSG_HEARTBEAT = 0x0002 	#心跳
	TERNIMAL_MSG_REGISTE = 0x0100		#注册
	TERNIMAL_MSG_LOGOUT = 0x0003		#注销
	TERNIMAL_MSG_AUTH = 0x0102			#鉴权
	TERNIMAL_MSG_LOCATION = 0x0200		#位置


	#服务器应答
	SERVER_RESP_COMMON = 0x8001		#通用应答
	SERVER_RESP_REGISTER = 0x8100	#注册应答


if __name__ == '__main__':
	print('Enum property:', JT808Const.TERNIMAL_MSG_REGISTE)
	print('Enum JT808Const.TERNIMAL_MSG_REGISTE value:', '0x%04x' % JT808Const.TERNIMAL_MSG_REGISTE.value)
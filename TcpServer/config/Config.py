# 日志配置

import logging

log2 = logging.getLogger("JT8082")
log2.setLevel(logging.INFO)

fh2 = logging.FileHandler("./logs/jt8082.log")
fh2.setLevel(logging.INFO)

ch2 =logging.StreamHandler()
ch2.setLevel(logging.INFO)

formatter2 = logging.Formatter('%(asctime)s---%(message)s')

fh2.setFormatter(formatter)
ch2.setFormatter(formatter)

log2.addHandler(fh2)
log2.addHandler(ch2)


# 服务配置

class BaseConfig(object):

	# 1.sqlite3 配置
	
	SQLITE_URI = r'sqlite:///datebase/JT808Api_1_0.db'

	# 2.redis 配置

	REDIS_HOST = r'localhost'
	REDIS_PORT = 6379
	REDIS_DB = 4


class DevConfig(BaseConfig):
	pass
	
	